package ru.tagirov.tm.command.system;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.Md5Util;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class RegistrationCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "reg";
    }

    @Override
    public @NonNull String description() {
        return "registration";
    }


    @Override
    public boolean isSecure() {
        return false;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, NoSuchAlgorithmException, SQLException {

        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @NotNull final List<User> userList = new ArrayList<>(userService.findAll());

        System.out.println("[USER REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().readLine();

        for (@NotNull final User user : userList) {
            if (user.getLogin().equals(login)) {
                System.out.println("LOGIN IS EXIST!");
                return;
            }
        }

        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = serviceLocator.getTerminalService().readLine();
        @NotNull final String passwordHash = Md5Util.getHash(password);

        serviceLocator.getTerminalService().printRoleField();
        @NotNull final String line = serviceLocator.getTerminalService().readLine();
        @NotNull Role role;
        switch (line) {
            case "1":
                role = Role.ADMIN;
                break;
            case "2":
                role = Role.MANAGER;
                break;
            case "3":
                role = Role.USER;
                break;
            default:
                System.out.println("Enter correct role");
                return;
        }

        @NotNull final User user = new User(UUID.randomUUID().toString(), login, passwordHash, role);
        userService.persist(user);
        System.out.println("[OK]\n");
    }
}

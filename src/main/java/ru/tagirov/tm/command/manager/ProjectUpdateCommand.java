package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;
import ru.tagirov.tm.util.StatusUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectUpdateCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project update";
    }

    @Override
    public @NonNull String description() {
        return "change project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws IOException, ParseException, SQLException {
        System.out.println("[PROJECT UPDATE]");

        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        serviceLocator.getTerminalService().printProjects(projects);

        System.out.println("ENTER NUMBER:");
        final int number = serviceLocator.getTerminalService().parseInt();

        if (number < 1 || number > projects.size()) {
            System.out.println("ENTER CORRECT NUMBER!");
            return;
        }

        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i) {
                selectField(projects.get(i));
                break;
            }
        }

        System.out.println("[OK]");
    }

    @NotNull
    private Project selectField(@NotNull final Project project) throws ParseException, IOException, SQLException {
        serviceLocator.getTerminalService().printProjectField();

        @NotNull final String fieldNumber = serviceLocator.getTerminalService().readLine();
        switch (fieldNumber) {
            case "1":
                System.out.println("Replace name to:");
                serviceLocator.getIProjectService().updateName(project.getId(), serviceLocator.getTerminalService().readLine());
                break;
            case "2":
                System.out.println("Replace description to:");
                serviceLocator.getIProjectService().updateDescription(project.getId(), serviceLocator.getTerminalService().readLine());
                break;
            case "3":
                System.out.println("Replace start date to:");
                serviceLocator.getIProjectService().updateDateCreate(project.getId(), DateUtil.getDate(serviceLocator.getTerminalService().readLine()));
                break;
            case "4":
                System.out.println("Replace end date to:");
                serviceLocator.getIProjectService().updateDateUpdate(project.getId(), DateUtil.getDate(serviceLocator.getTerminalService().readLine()));
                break;
            case "5":
                System.out.println("Replace status to:");
                serviceLocator.getTerminalService().printStatusField();
                serviceLocator.getIProjectService().updateStatus(project.getId(), StatusUtil.changeStatus(serviceLocator.getTerminalService().readLine()));
                break;
            default:
                System.out.println("Nothing has changed!");
                break;
        }

        return project;
    }
}

package ru.tagirov.tm.command.manager;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectRemoveCommand extends AbstractCommand {

    @Override
    public void setServiceLocator(ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
    }

    @Override
    public @NonNull String command() {
        return "project remove";
    }

    @Override
    public @NonNull String description() {
        return "delete one project";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.MANAGER;
    }

    @Override
    public void execute() throws IOException, SQLException {

        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final List<User> users = new ArrayList<>(userService.findAll());
        System.out.println("[PROJECT REMOVE]\nCHOOSE THE RIGHT USER: ");
        @NotNull final User user = serviceLocator.getTerminalService().getUser();
        @NotNull final List<Project> projects = new ArrayList<>(Objects.requireNonNull(projectService.findAllByUserId(user.getId())));
        for (Project p : projects){
            System.out.println(p.getUserId());
            System.out.println(p.getId());
        }

        serviceLocator.getTerminalService().printProjects(projects);

        System.out.println("ENTER NUMBER:[0-Non-Project]");
        final int number = serviceLocator.getTerminalService().parseInt();

        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return;
        }

        if (number == 0) {
            System.out.println("Non-project tasks remove");
            taskService.removeAllByProjectId(user.getId(), null);
            System.out.println("[OK]");
            return;
        }

        for (int i = 0; i < projects.size(); i++) {
            if ((number - 1) == i) {
                @NotNull final Project project = Objects.requireNonNull(projectService.removeByUserId(user.getId(), projects.get(i).getId()));
                taskService.removeAllByProjectId(user.getId(), project.getId());
            }
        }

        System.out.println("[OK]\n");
    }
}

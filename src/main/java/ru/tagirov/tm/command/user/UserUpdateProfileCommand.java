package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "update login";
    }

    @Override
    public @NonNull String description() {
        return "update login";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {

        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @NotNull final User user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());

        System.out.println("[UPDATE LOGIN]");
        System.out.println("ENTER NEW LOGIN:");
        @NotNull final String login = serviceLocator.getTerminalService().readLine();

        userService.updateLogin(user.getId(), login);
        System.out.println("LOGIN IS EXIST!");
        System.out.println("[OK]");
    }
}


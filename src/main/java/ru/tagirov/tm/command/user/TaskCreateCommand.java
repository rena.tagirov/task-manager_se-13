package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.PurposeStatus;
import ru.tagirov.tm.enumeration.Role;
import ru.tagirov.tm.util.DateUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "task create";
    }

    @Override
    public @NonNull String description() {
        return "create new task";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, ParseException, SQLException {
        System.out.println("[TASK CREATE]");
        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        @NotNull final List<Project> projects = new ArrayList<>(projectService.findAllByUserId(user.getId()));

        serviceLocator.getTerminalService().printProjects(projects);
        System.out.println("WHICH PROJECT(0 - non project):");

        final int number = serviceLocator.getTerminalService().parseInt();
        @Nullable String idProject = null;
        if (number < 0 || number > projects.size()) {
            System.out.println("Enter correct number!");
            return;
        } else if(number != 0)
            idProject = projects.get(number - 1).getId();

        System.out.println("ENTER NAME:");
        @NotNull final String name = serviceLocator.getTerminalService().readLine();

        System.out.println("ENTER DESCRIPTION");
        @NotNull final String description = serviceLocator.getTerminalService().readLine();

        System.out.println("START TIME(DD-MM-YYYY):");
        @NotNull final Date beginDate = DateUtil.getDate(serviceLocator.getTerminalService().readLine());

        System.out.println("END TIME(DD-MM-YYYY):");
        @NotNull final Date endDate = DateUtil.getDate(serviceLocator.getTerminalService().readLine());

        @NotNull final String userId = user.getId();
        @NotNull final Task task = new Task(UUID.randomUUID().toString(), name, description, beginDate, endDate, idProject, userId, PurposeStatus.PLANNED);
        taskService.persist(task);
        System.out.println("[OK]");
    }
}

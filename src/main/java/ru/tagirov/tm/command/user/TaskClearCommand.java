package ru.tagirov.tm.command.user;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;

public class TaskClearCommand extends AbstractCommand {


    @Override
    public @NonNull String command() {
        return "task clear";
    }

    @Override
    public @NonNull String description() {
        return "delete all tasks";
    }


    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {

        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }
            taskService.removeAllByUserId(user.getId());
        System.out.println("[ALL TASK REMOVE]");
    }
}

package ru.tagirov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

public final class UserRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user remove";
    }

    @NotNull
    @Override
    public String description() {
        return "remove your account";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @NotNull
    @Override
    public Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws SQLException, IOException {
        System.out.println("[USER REMOVE]");

        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        projectService.removeAllByUserId(user.getId());
        taskService.removeAllByUserId(user.getId());
        userService.remove(user.getId());
        System.out.println("[OK]\n");
    }
}

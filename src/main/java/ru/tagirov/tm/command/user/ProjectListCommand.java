package ru.tagirov.tm.command.user;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.PurposeComparator;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public @NonNull String command() {
        return "project list";
    }

    @NotNull
    @Override
    public @NonNull String description() {
        return "see all projects";
    }

    @Override
    public boolean isSecure() {
        return true;
    }


    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {
        System.out.println("[PROJECT LIST]");

        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }

        @NotNull final List<Project> projectList = getSortedList(new ArrayList<>(projectService.findAllByUserId(user.getId())));


        for (int i = 0; i < projectList.size(); i++) {
            @NotNull final Project project = projectList.get(i);
            System.out.println("Project #" + (i + 1) + ".");
            serviceLocator.getTerminalService().printProject(project);
        }
    }

    @NotNull
    private List<Project> getSortedList(@NotNull final List<Project> projectList) throws IOException {
        serviceLocator.getTerminalService().printHowSorted();
        switch (serviceLocator.getTerminalService().readLine()) {
            case "1":
                projectList.sort(PurposeComparator.DATE_BEGIN_COMPARATOR.getComparator());
                break;
            case "2":
                projectList.sort(PurposeComparator.DATE_END_COMPARATOR.getComparator());
                break;
            case "3":
                projectList.sort(PurposeComparator.STATUS_COMPARATOR.getComparator());
                break;
            default:
                System.out.println("Insertion order!");
        }
        return projectList;
    }
}

package ru.tagirov.tm.command.user;
import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.enumeration.PurposeComparator;
import ru.tagirov.tm.enumeration.Role;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskListCommand extends AbstractCommand {

    @Override
    public @NonNull String command() {
        return "task list";
    }

    @Override
    public @NonNull String description() {
        return "see all tasks";
    }

    @Override
    public boolean isSecure() {
        return true;
    }

    @Override
    public @NotNull Role getRole() {
        return Role.USER;
    }

    @Override
    public void execute() throws IOException, SQLException {

        System.out.println("[TASK LIST]");
        @NotNull final IProjectService projectService = serviceLocator.getIProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getITaskService();
        @NotNull final User user;
        if (serviceLocator.getIUserService().getCurrentUser().getRole() == Role.USER){
            user = Objects.requireNonNull(serviceLocator.getIUserService().getCurrentUser());
        }else {
            user = serviceLocator.getTerminalService().getUser();
        }


        @NotNull final List<Task> tasks = getSortedList(new ArrayList<>(taskService.findAllByUserId(user.getId())));

        @NotNull final List<Project> projects = new ArrayList<>(Objects.requireNonNull(projectService.findAllByUserId(user.getId())));

        for (int i = 0; i < tasks.size(); i++) {
            @NotNull final Task task = tasks.get(i);
            @NotNull String projectName = "Non-project";

            for (@NotNull final Project project : projects) {
                if (project.getId().equals(task.getIdProject())) {
                    projectName = project.getName();
                }
            }
            System.out.println("Task #" + (i + 1) + ".");
            serviceLocator.getTerminalService().printTask(projectName, task);
        }
    }

    @NotNull
    private List<Task> getSortedList(@NotNull final List<Task> taskList) throws IOException {
        serviceLocator.getTerminalService().printHowSorted();
        switch (serviceLocator.getTerminalService().readLine()) {
            case "1":
                taskList.sort(PurposeComparator.DATE_BEGIN_COMPARATOR.getComparator());
                break;
            case "2":
                taskList.sort(PurposeComparator.DATE_END_COMPARATOR.getComparator());
                break;
            case "3":
                taskList.sort(PurposeComparator.STATUS_COMPARATOR.getComparator());
                break;
            default:
                System.out.println("Insertion order!");
        }
        return taskList;
    }

}

package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.PurposeStatus;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
public class Task extends PurposeEntity implements Serializable {

    private static final long serialVersionUID = 403527600450344261L;

    @Nullable
    private String idProject;

    public Task(@NonNull final String id,
                @NonNull final String name,
                @NonNull final String description,
                @NonNull final Date dateCreate,
                @NonNull final Date dateUpdate,
                @NonNull final String idProject,
                @NonNull final String userId,
                @NotNull final PurposeStatus purposeStatus) {
        super(id, name, description, dateCreate, dateUpdate, userId, purposeStatus);
        this.idProject = idProject;

    }
}
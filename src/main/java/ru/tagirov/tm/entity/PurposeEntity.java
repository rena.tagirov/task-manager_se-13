package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.PurposeStatus;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class PurposeEntity extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503442617L;

    @NonNull
    private String name;

    @NonNull
    private String description;

    @NonNull
    private Date dateCreate;

    @Nullable
    private Date dateUpdate;

    @NonNull
    private String userId;

    @NotNull
    private PurposeStatus purposeStatus;

    public PurposeEntity(@NotNull final String id,
                         @NotNull final String name,
                         @NotNull final String description,
                         @NotNull final Date dateCreate,
                         @NotNull final Date dateUpdate,
                         @NotNull final String userId,
                         @NotNull final PurposeStatus purposeStatus) {
        super(id);
        this.name = name;
        this.description = description;
        this.dateCreate = dateCreate;
        this.dateUpdate = dateUpdate;
        this.userId = userId;
        this.purposeStatus = purposeStatus;
    }
    public void setPurposeStatus(String line) {
        PurposeStatus purposeStatus = null;
        switch (line) {
            case "PLANNED":
                purposeStatus = PurposeStatus.PLANNED;
                break;
            case "planned":
                purposeStatus = PurposeStatus.PLANNED;
                break;
            case "DURING":
                purposeStatus = PurposeStatus.DURING;
                break;
            case "during":
                purposeStatus = PurposeStatus.DURING;
                break;
            case "READY":
                purposeStatus = PurposeStatus.READY;
                break;
            case "ready":
                purposeStatus = PurposeStatus.READY;
                break;
        }
        this.purposeStatus = purposeStatus;
    }

}

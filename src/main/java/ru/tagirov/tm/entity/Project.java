package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.PurposeStatus;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
public class Project extends PurposeEntity implements Serializable {

    private static final long serialVersionUID = -1243348624045946182L;

    public Project(@NonNull final String id,
                   @NonNull final String name,
                   @NonNull final String description,
                   @NonNull final Date dateCreate,
                   @NonNull final Date dateUpdate,
                   @NonNull final String userId,
                   @NotNull final PurposeStatus purposeStatus){
        super(id, name, description, dateCreate, dateUpdate, userId, purposeStatus);
    }
}

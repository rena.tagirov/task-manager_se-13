package ru.tagirov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.enumeration.Role;

import java.io.Serializable;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@Getter
@Setter
public class User extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503441123L;

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private Role role;

    public User(@NonNull final String id,
                @NonNull final String login,
                @NonNull final String password,
                @NonNull final Role role) {
        super(id);
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public void setRole(String line) {
        Role role = null;
        switch (line) {
            case "ADMIN":
                role = Role.ADMIN;
                break;
            case "admin":
                role = Role.ADMIN;
                break;
            case "MANAGER":
                role = Role.MANAGER;
                break;
            case "manager":
                role = Role.MANAGER;
                break;
            case "USER":
                role = Role.USER;
                break;
            case "user":
                role = Role.USER;
                break;
        }
        this.role = role;
    }
}

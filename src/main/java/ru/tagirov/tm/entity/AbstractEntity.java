package ru.tagirov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1498399422226809080L;

    @NotNull
    private String id;

    public AbstractEntity(@NotNull final String id){
        this.id = id;
    }
}

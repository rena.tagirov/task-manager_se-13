package ru.tagirov.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.entity.PurposeEntity;

import java.util.Comparator;

public class StatusComparator implements Comparator<PurposeEntity> {
    @Override
    public int compare(@NotNull final PurposeEntity a, @NotNull final PurposeEntity b) {
        return Integer.compare(a.getPurposeStatus().getWeight(), b.getPurposeStatus().getWeight());
    }
}

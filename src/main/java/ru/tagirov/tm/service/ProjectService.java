package ru.tagirov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.ProjectMapper;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.util.Md5Util;
import ru.tagirov.tm.util.MyBatisUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
public class ProjectService implements IProjectService {

    @NotNull
    private final SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();

    @NotNull
    private final ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull Project project) throws SQLException {
        try {
            projectMapper.persist(project);
            session.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            session.rollback();
        }
    }

    @Override
    public @Nullable Project merge(@NotNull Project project) throws SQLException {
        projectMapper.merge(project);
        session.commit();
        return findOne(project.getId());
    }

    @Override
    public @Nullable Project findOne(@NotNull String uuid) throws SQLException {
        return projectMapper.findOne(uuid);
    }

    @Override
    public @NotNull Collection<Project> findAll() throws SQLException {
        return projectMapper.findAll();
    }

    @Override
    public @Nullable Project remove(@NotNull String uuid) throws SQLException {
        projectMapper.remove(uuid);
        session.commit();
        return findOne(uuid);
    }

    @Override
    public void removeAll() throws SQLException {
        projectMapper.removeAll();
        session.commit();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable Project findOneByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        return projectMapper.findOneByUserId(userId, uuid);
    }

    @Override
    public @NotNull Collection<Project> findAllByUserId(@NotNull String userId) throws SQLException {
        return Objects.requireNonNull(projectMapper.findAllByUserId(userId));
    }

    @Override
    public @Nullable Project removeByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        Project project = findOneByUserId(userId, uuid);
        projectMapper.removeByUserId(userId, uuid);
        session.commit();
        return project;
    }

    @Override
    public void removeAllByUserId(@NotNull String userId) throws SQLException {
        projectMapper.removeAllByUserId(userId);
        session.commit();
    }

    @Override
    public @NotNull Collection<Project> findAllByLine(@NotNull String userId, @NotNull String line) throws SQLException {
        return projectMapper.findAllByLine(userId, line);
    }

    //    UPDATE -----------------------------------------------------------

    @Override
    public void updateName(@NotNull String id, @NotNull String line) throws SQLException{
        projectMapper.updateName(id, line);
        session.commit();
    }

    @Override
    public void updateDescription(@NotNull String id, @NotNull String line) throws SQLException{
        projectMapper.updateDescription(id, line);
        session.commit();
    }

    @Override
    public void updateDateCreate(@NotNull String id, @NotNull Date line) throws SQLException{
        projectMapper.updateDateCreate(id, line);
        session.commit();
    }

    @Override
    public void updateDateUpdate(@NotNull String id, @NotNull Date line) throws SQLException{
        projectMapper.updateDateUpdate(id, line);
        session.commit();

    }
    @Override
    public void updateStatus(@NotNull String id, @NotNull String line) throws SQLException{
        projectMapper.updateStatus(id, line);
        session.commit();
    }
}


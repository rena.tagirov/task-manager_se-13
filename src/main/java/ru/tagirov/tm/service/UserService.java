package ru.tagirov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.UserMapper;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.util.MyBatisUtil;

import java.sql.SQLException;
import java.util.Collection;

@NoArgsConstructor
public class UserService implements IUserService {

    @NotNull
    private final SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();

    @NotNull
    private final UserMapper userMapper = session.getMapper(UserMapper.class);

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull User user) throws SQLException {
        try {
            userMapper.persist(user);
            session.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            session.rollback();
        }
    }

    @Override
    public @Nullable User merge(@NotNull User user) throws SQLException {
        userMapper.merge(user);
        session.commit();
        return findOne(user.getId());

    }

    @Override
    public @Nullable User findOne(@NotNull String id) throws SQLException {
        return userMapper.findOne(id);

    }

    @Override
    public @NotNull Collection<User> findAll() throws SQLException {
        return userMapper.findAll();
    }

    @Override
    public @Nullable User remove(@NotNull String id) throws SQLException {
        userMapper.remove(id);
        session.commit();
        return findOne(id);

    }

    @Override
    public void removeAll() throws SQLException {
        userMapper.removeAll();
        session.commit();
    }

    //    ALL ------------------------------------------------------------------------

    @Nullable
    private User user = null;

    @Override
    public @Nullable User getCurrentUser() {
        return user;
    }

    @Override
    public void setCurrentUser(@Nullable User user) {
        this.user = user;
    }

    //    UPDATE -----------------------------------------------------------

    @Override
    public void updateLogin(@NotNull String userId, @NotNull String login) {
        userMapper.updateLogin(userId, login);
        session.commit();
    }

    @Override
    public void update(@NotNull final String id, @NotNull final String line) throws SQLException {
        userMapper.update(id, line);
        session.commit();
    }
}

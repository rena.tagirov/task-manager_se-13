package ru.tagirov.tm.service;

import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.api.repository.TaskMapper;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.util.MyBatisUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

@NoArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    private final SqlSession session = MyBatisUtil.getSqlSessionFactory().openSession();

    @NotNull
    private final TaskMapper taskMapper = session.getMapper(TaskMapper.class);

    //    CRUD ----------------------------------------------------------------------

    @Override
    public void persist(@NotNull Task task) throws SQLException {
        try {
            taskMapper.persist(task);
            session.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            session.rollback();
        }
    }

    @Override
    public @Nullable Task merge(@NotNull Task task) throws SQLException {
        taskMapper.merge(task);
        session.commit();
        return findOne(task.getId());
    }

    @Override
    public @Nullable Task findOne(@NotNull String uuid) throws SQLException {
        return taskMapper.findOne(uuid);
    }

    @Override
    public @NotNull Collection<Task> findAll() throws SQLException {
        return taskMapper.findAll();
    }

    @Override
    public @Nullable Task remove(@NotNull String uuid) throws SQLException {
        taskMapper.remove(uuid);
        session.commit();
        return findOne(uuid);
    }

    @Override
    public void removeAll() throws SQLException {
        taskMapper.removeAll();
        session.commit();
    }

    //    ALL ------------------------------------------------------------------------

    @Override
    public @Nullable Task findOneByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        return taskMapper.findOneByUserId(userId, uuid);
    }

    @Override
    public @NotNull Collection<Task> findAllByUserId(@NotNull String userId) throws SQLException {
        return taskMapper.findAllByUserId(userId);
    }

    @Override
    public @Nullable Task removeByUserId(@NotNull String userId, @NotNull String uuid) throws SQLException {
        Task task = findOneByUserId(userId, uuid);
        taskMapper.removeByUserId(userId, uuid);
        session.commit();
        return task;    }

    @Override
    public void removeAllByUserId(@NotNull String userId) throws SQLException {
        taskMapper.removeAllByUserId(userId);
        session.commit();
    }

    @Override
    public @NotNull Collection<Task> findAllByLine(@NotNull String userId, @NotNull String line) throws SQLException {
        return taskMapper.findAllByLine(userId, line);
    }

    @Override
    public Collection<Task> findAllByProjectId(String userId, @Nullable String projectId) throws SQLException {
        return taskMapper.findAllByProjectId(userId, projectId);
    }

    @Override
    public void removeAllByProjectId(String userId, @Nullable String projectId) throws SQLException {
        taskMapper.removeAllByProjectId(userId, projectId);
        session.commit();
    }

    //    UPDATE -----------------------------------------------------------

    @Override
    public void updateName(@NotNull String id, @NotNull String line) throws SQLException{
        taskMapper.updateName(id, line);
        session.commit();
    }

    @Override
    public void updateDescription(@NotNull String id, @NotNull String line) throws SQLException{
        taskMapper.updateDescription(id, line);
        session.commit();
    }

    @Override
    public void updateDateCreate(@NotNull String id, @NotNull Date line) throws SQLException{
        taskMapper.updateDateCreate(id, line);
        session.commit();
    }

    @Override
    public void updateDateUpdate(@NotNull String id, @NotNull Date line) throws SQLException{
        taskMapper.updateDateUpdate(id, line);
        session.commit();
    }

    @Override
    public void updateStatus(@NotNull String id, @NotNull String line) throws SQLException{
        taskMapper.updateStatus(id, line);
        session.commit();
    }

    @Override
    public void updateProjectToTask(@NotNull String id, @NotNull String line) throws SQLException{
        taskMapper.updateProjectToTask(id, line);
        session.commit();
    }
}

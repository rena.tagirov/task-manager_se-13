package ru.tagirov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {
    ADMIN(3, "admin"),
    MANAGER(2, "manager"),
    USER(1, "user");

    final int lvl;
    @NotNull String role;

    Role(final int lvl, @NotNull final String role) {
        this.lvl = lvl;
        this.role = role;
    }

    public int getLvl() {
        return lvl;
    }

    @NotNull
    public String getRole() {
        return role;
    }
}

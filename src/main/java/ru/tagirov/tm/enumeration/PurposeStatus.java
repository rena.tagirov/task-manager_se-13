package ru.tagirov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum PurposeStatus {

    PLANNED("planned", 1),
    DURING("during", 2),
    READY("ready", 3);

    @NotNull
    private final String status;

    private final int weight;

    PurposeStatus(@NotNull final String status, final int weight) {
        this.status = status;
        this.weight = weight;
    }

    @NotNull
    public String getStatus() {
        return status;
    }

    public int getWeight() {
        return weight;
    }

}
package ru.tagirov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.PurposeStatus;

public final class StatusUtil {

    @NotNull
    public static String changeStatus(@NotNull final String line) {
        PurposeStatus purposeStatus = null;
        switch (line) {
            case "1":
                purposeStatus = PurposeStatus.PLANNED;
                break;
            case "2":
                purposeStatus = PurposeStatus.DURING;
                break;
            case "3":
                purposeStatus = PurposeStatus.READY;
                break;
            default:
                System.out.println("Status don't changed!");
        }

        return purposeStatus.getStatus();
    }

}

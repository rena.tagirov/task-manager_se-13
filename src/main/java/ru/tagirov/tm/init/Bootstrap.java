package ru.tagirov.tm.init;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.api.repository.ProjectMapper;
import ru.tagirov.tm.api.repository.TaskMapper;
import ru.tagirov.tm.api.repository.UserMapper;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.command.AbstractCommand;
import ru.tagirov.tm.service.ProjectService;
import ru.tagirov.tm.service.TaskService;
import ru.tagirov.tm.service.UserService;
import ru.tagirov.tm.terminal.TerminalService;
import ru.tagirov.tm.util.MyBatisUtil;

import java.security.NoSuchAlgorithmException;
import java.util.*;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final IProjectService projectService = new ProjectService();

    @NotNull
    private final ITaskService taskService = new TaskService();

    @NotNull
    private final IUserService userService = new UserService();

    @NotNull
    private final Set<Class<? extends AbstractCommand>> commandClasses = getAbstractCommandClasses();

    private void registry(@NotNull final AbstractCommand command) {
        @NotNull final String cliCommand = command.command();

        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    private void init() throws IllegalAccessException, InstantiationException, NoSuchAlgorithmException {
        for (@NotNull final Class<? extends AbstractCommand> abstractClass : commandClasses) {
            registry(abstractClass.newInstance());
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("No such command!");
            return;
        }
        if (userService.getCurrentUser() == null && !abstractCommand.isSecure()) {
            abstractCommand.execute();
            return;
        }
        if (userService.getCurrentUser() != null && abstractCommand.isSecure() && (abstractCommand.getRole().getLvl() <= userService.getCurrentUser().getRole().getLvl())) {
            abstractCommand.execute();
            return;
        }
        if ("help".equals(abstractCommand.command()) || "about".equals(abstractCommand.command())) {
            abstractCommand.execute();
            return;
        }
        System.out.println("No such command!");
    }

    private Set<Class<? extends AbstractCommand>> getAbstractCommandClasses() {
        return new Reflections("ru.tagirov.tm").getSubTypesOf(AbstractCommand.class);
    }

    public void start() throws Exception {
        init();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.println("\"help\" - get access command.");
        System.out.println();
        @Nullable String command;

        while (true) {
            command = getTerminalService().readLine();
            if ("EXIT".equals(command))
                return;
            try {
                execute(command);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @NotNull
    public TerminalService getTerminalService() {
        return new TerminalService(this);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public IProjectService getIProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ITaskService getITaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IUserService getIUserService() {
        return userService;
    }
}
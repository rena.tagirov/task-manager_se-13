package ru.tagirov.tm.terminal;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.api.ServiceLocator;
import ru.tagirov.tm.api.service.IProjectService;
import ru.tagirov.tm.api.service.ITaskService;
import ru.tagirov.tm.api.service.IUserService;
import ru.tagirov.tm.entity.Project;
import ru.tagirov.tm.entity.Task;
import ru.tagirov.tm.entity.User;
import ru.tagirov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TerminalService {

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    protected ServiceLocator serviceLocator;

    public TerminalService() {
    }
    public TerminalService(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @NonNull
    public String readLine() throws IOException {
        return reader.readLine();
    }

    public int parseInt() throws IOException {
        return Integer.parseInt(reader.readLine());
    }

    public User getUser() throws SQLException, IOException {
        final TerminalService t = new TerminalService();
        @NotNull final IUserService userService = serviceLocator.getIUserService();
        @NotNull final List<User> users = new ArrayList<>(userService.findAll());
        System.out.println("CHOOSE THE RIGHT USER: ");
        int count = 1;
        for (User user1 : users) {
            System.out.print(count + ": " );
            serviceLocator.getTerminalService().printUser(user1);
            count++;
        }
        final int numberUser = t.parseInt();
        User user = Objects.requireNonNull(users.get(numberUser - 1));
        return user;
    }

    public void printProject(final Project project){
        System.out.println(new StringBuilder()
                .append("\nName: ").append(project.getName())
                .append("\nDescription: ").append(project.getDescription())
                .append("\nStart date: ").append(project.getDateCreate())
                .append("\nEnd date: ").append(project.getDateUpdate())
                .append("\n--------------------------------------------------"));
    }

    public void printTask(@NotNull final String projectName, @NotNull final Task task) {
        System.out.println(new StringBuilder()
                .append("\nProject Name: ").append(projectName)
                .append("\nName: ").append(task.getName())
                .append("\nDescription: ").append(task.getDescription())
                .append("\nStart date: ").append(DateUtil.getDate(task.getDateCreate()))
                .append("\nEnd date: ").append(DateUtil.getDate(task.getDateUpdate()))
                .append("\n--------------------------------------------------"));
    }

    public void printTask(@NotNull final Task task) {
        System.out.println(new StringBuilder()
                .append("\nName: ").append(task.getName())
                .append("\nDescription: ").append(task.getDescription())
                .append("\nStart date: ").append(DateUtil.getDate(task.getDateCreate()))
                .append("\nEnd date: ").append(DateUtil.getDate(task.getDateUpdate()))
                .append("\n--------------------------------------------------"));
    }

    public void printUser(@NotNull final User user) {
        System.out.println(new StringBuilder()
                .append(user.getLogin())
                .append("\nRole: ")
                .append(user.getRole().getRole())
                .append("\n--------------------------------------------------"));
    }

    public void printRoleField(){
        System.out.println(new StringBuilder().append("ENTER ROLE:")
                .append("\n    1. admin")
                .append("\n    2. manager")
                .append("\n    3. user"));
    }

    public void printProjects(@NotNull final List<Project> projects) {
        int count = 0;
        for (Project project : projects) {
            System.out.println(++count + ". " + project.getName());
        }
    }

    public void printTasks(@NotNull final List<Task> tasks) {
        int count = 0;
        for (Task task : tasks) {
            System.out.println(++count + ". " + task.getName());
        }
    }

    public void printProjectField() {
        System.out.println(new StringBuilder().append("What do you want to change? ")
                .append("\n 1. Name")
                .append("\n 2. Description")
                .append("\n 3. Start date")
                .append("\n 4. End date")
                .append("\n 5. Status"));

    }

    public void printTaskField() {
        System.out.println(new StringBuilder().append("What do you want to change? ")
                .append("\n 1. Name")
                .append("\n 2. Description")
                .append("\n 3. Start date")
                .append("\n 4. End date")
                .append("\n 5. Status")
                .append("\n 6. Project"));

    }

    public void printStatusField(){
        System.out.println(new StringBuilder().append("Select status:")
                .append("\n 1.planned")
                .append("\n 2.during")
                .append("\n 3.ready"));
    }

    public void printHowSorted() {
        System.out.println(new StringBuilder()
                .append("How sorted:")
                .append("\n 1.Start date")
                .append("\n 2.End date")
                .append("\n 3.Status"));
    }

    public void printSaveField() {
        System.out.println(new StringBuilder()
                .append("How to save:")
                .append("\n1. Binary")
                .append("\n2. XML")
                .append("\n3. JSON")
                .append("\n4. XML-Jaxb")
                .append("\n5. JSON-Jaxb"));
    }

    public void printLoadField() {
        System.out.println(new StringBuilder()
                .append("How to load:")
                .append("\n1. Binary")
                .append("\n2. XML")
                .append("\n3. JSON")
                .append("\n4. XML-Jaxb")
                .append("\n5. JSON-Jaxb"));
    }

}

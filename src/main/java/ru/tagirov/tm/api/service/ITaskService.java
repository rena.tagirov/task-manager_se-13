package ru.tagirov.tm.api.service;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface ITaskService {

    //    CRUD ----------------------------------------------------------------------

    void persist(@NotNull final Task task) throws SQLException;

    @Nullable
    Task merge(@NotNull final Task task) throws SQLException;

    @Nullable
    Task findOne(@NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<Task> findAll() throws SQLException;

    @Nullable
    Task remove(@NotNull final String uuid) throws SQLException;

    void removeAll() throws SQLException;

    //    ALL ------------------------------------------------------------------------

    @Nullable
    Task findOneByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<Task> findAllByUserId(@NotNull final String userId) throws SQLException;

    @Nullable
    Task removeByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Collection<Task> findAllByLine(final @NotNull String userId, final @NotNull String line) throws SQLException;

    @NotNull
    Collection<Task> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws SQLException;

    void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) throws SQLException;

    //    UPDATE -----------------------------------------------------------

    void updateName(@NotNull String id, @NotNull String line) throws SQLException;

    void updateDescription(@NotNull String id, @NotNull String line) throws SQLException;

    void updateDateCreate(@NotNull String id, @NotNull Date line) throws SQLException;

    void updateDateUpdate(@NotNull String id, @NotNull Date line) throws SQLException;

    void updateStatus(@NotNull String id, @NotNull String line) throws SQLException;

    void updateProjectToTask(@NotNull String id, @NotNull String line) throws SQLException;
}

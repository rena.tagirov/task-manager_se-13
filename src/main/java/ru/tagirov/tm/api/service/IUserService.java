package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.AbstractEntity;
import ru.tagirov.tm.entity.User;

import javax.jws.soap.SOAPBinding;
import java.sql.SQLException;
import java.util.Collection;

public interface IUserService {

//    CRUD ----------------------------------------------------------------------

    void persist(@NotNull final User user) throws SQLException;

    @Nullable
    User merge(@NotNull final User user) throws SQLException;

    @Nullable
    User findOne(@NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<User> findAll() throws SQLException;

    @Nullable
    User remove(@NotNull final String uuid) throws SQLException;

    void removeAll() throws SQLException;

//    ALL ------------------------------------------------------------------------

    @Nullable User getCurrentUser();

     void setCurrentUser(@Nullable User user);

    //    UPDATE -----------------------------------------------------------

    void updateLogin(@NotNull String userId, @NotNull String login);

    void update(@NotNull final String id, @NotNull final String line) throws SQLException;
}

package ru.tagirov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Project;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface IProjectService {

    //    CRUD ----------------------------------------------------------------------

    void persist(@NotNull final Project project) throws SQLException;

    @Nullable
    Project merge(@NotNull final Project project) throws SQLException;

    @Nullable
    Project findOne(@NotNull final String uuid) throws SQLException;

    @NotNull
    Collection<Project> findAll() throws SQLException;

    @Nullable
    Project remove(@NotNull final String uuid) throws SQLException;

    void removeAll() throws SQLException;

    //    ALL ------------------------------------------------------------------------

    @Nullable
    Project findOneByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    @Nullable
    Collection<Project> findAllByUserId(@NotNull final String userId) throws SQLException;

    @Nullable
    Project removeByUserId(@NotNull final String userId, @NotNull final String uuid) throws SQLException;

    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Collection<Project> findAllByLine(final @NotNull String userId, final @NotNull String line) throws SQLException;

    //    UPDATE -----------------------------------------------------------

    void updateName(@NotNull String id, @NotNull String line) throws SQLException;

    void updateDescription(@NotNull String id, @NotNull String line) throws SQLException;

    void updateDateCreate(@NotNull String id, @NotNull Date line) throws SQLException;

    void updateDateUpdate(@NotNull String id, @NotNull Date line) throws SQLException;

    void updateStatus(@NotNull String id, @NotNull String line) throws SQLException;
}

package ru.tagirov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.User;

import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.util.Collection;

public interface UserMapper {

//     CRUD ------------------------------------------------------------------------------

    @Insert("INSERT INTO app_user (id ,login , password, role) VALUES (#{id},#{login},#{password},#{role})")
    void persist(@NotNull final User user) throws SQLException;

    @Insert("INSERT INTO app_user (id ,login , password, role) VALUES (#{id},#{login},#{password},#{role})")
    @Nullable
    User merge(@NotNull final User user) throws SQLException;

    @Select("SELECT * FROM app_user WHERE id = #{id}")
    @Nullable
    User findOne(@NotNull final String id) throws SQLException;

    @Select("SELECT * FROM app_user")
    @NotNull
    Collection<User> findAll() throws SQLException;

    @Delete("DELETE FROM app_user WHERE id = #{id}")
    void remove(@NotNull final String id) throws SQLException;

    @Delete("DELETE FROM app_user")
    void removeAll() throws SQLException;

//    ALL -------------------------------------------------------------------------------

    @Update("UPDATE app_user SET login = #{login} WHERE id = #{id};")
    void updateLogin(@Param("id") @NotNull String userId, @Param("login") @NotNull final String login);

//    UPDATE -------------------------------------------------------------------------

    @Update("UPDATE app_user SET password = #{password} WHERE id = #{id};")
    void update(@Param("id")String id, @Param("password") String password) throws SQLException;
}

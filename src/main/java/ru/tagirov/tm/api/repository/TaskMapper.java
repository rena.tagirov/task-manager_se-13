package ru.tagirov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Task;

import javax.validation.constraints.NotNull;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;


public interface TaskMapper {

//    CRUD------------------------------------------------------------------------
    @Insert("INSERT INTO app_task (id ,name , description, date_create, date_update, project_id, user_id, purpose_status) VALUES (#{id},#{name},#{description},#{dateCreate}, #{dateUpdate}, #{idProject}, #{userId}, #{purposeStatus})")
    void persist(@NotNull final Task task) throws SQLException;

    @Insert("INSERT INTO app_task (id ,name , description, date_create, date_update, project_id, user_id, purpose_status) VALUES (#{id},#{name},#{description},#{dateCreate}, #{dateUpdate}, #{idProject}, #{userId}, #{purposeStatus})")
    void merge(@NotNull final Task task) throws SQLException;

    @Select("SELECT * FROM app_task WHERE id = #{id}")
    @Nullable
    Task findOne(@NotNull final String id) throws SQLException;

    @Select("SELECT * FROM app_task")
    @NotNull
    Collection<Task> findAll() throws SQLException;

    @Delete("DELETE FROM app_task WHERE id = #{id}")
    void remove(@NotNull final String id) throws SQLException;

    @Delete("DELETE FROM app_task")
    void removeAll() throws SQLException;

//    ALL -----------------------------------------------------

    @Select("SELECT * FROM app_task WHERE id = #{id} AND user_id = #{userId}")
    @Nullable
    Task findOneByUserId(@Param ("userId")@NotNull final String userId, @Param ("id")@NotNull final String id) throws SQLException;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "dateCreate", column = "date_create"),
            @Result(property = "dateUpdate", column = "date_update"),
            @Result(property = "idProject", column = "project_id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "purposeStatus", column = "purpose_status")
    })
    @NotNull
    Collection<Task> findAllByUserId(@NotNull final String userId) throws SQLException;

    @Delete("DELETE FROM app_task WHERE id = #{id} AND user_id = #{userId}")
    void removeByUserId(@Param ("userId") @NotNull final String userId, @Param ("id")@NotNull final String id) throws SQLException;

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    @NotNull
    Collection<Task> findAllByLine(final @NotNull String userId, final @NotNull String line) throws SQLException;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    @NotNull
    Collection<Task> findAllByProjectId(@Param ("userId") @NotNull final String userId,@Param ("projectId") @Nullable final String projectId) throws SQLException;

    @Delete("DELETE FROM app_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllByProjectId(@Param ("userId") @NotNull final String userId, @Param ("projectId") @Nullable final String projectId) throws SQLException;

//    Update---------------------------------------

    @Update("UPDATE app_task SET name = #{line} WHERE id = #{id}")
    void updateName(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;

    @Update("UPDATE app_task SET description = #{line} WHERE id = #{id}")
    void updateDescription(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;

    @Update("UPDATE app_task SET date_create = #{line} WHERE id = #{id}")
    void updateDateCreate(@Param("id")@NotNull String id, @Param("line") @NotNull Date line) throws SQLException;

    @Update("UPDATE app_task SET date_update = #{line} WHERE id = #{id}")
    void updateDateUpdate(@Param("id")@NotNull String id, @Param("line") @NotNull Date line) throws SQLException;

    @Update("UPDATE app_task SET purpose_status = #{line} WHERE id = #{id}")
    void updateStatus(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;

    @Update("UPDATE app_task SET project_id = #{line} WHERE id = #{id}")
    void updateProjectToTask(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;

}

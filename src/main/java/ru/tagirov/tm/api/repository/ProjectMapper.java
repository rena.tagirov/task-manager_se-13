package ru.tagirov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tagirov.tm.entity.Project;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;

public interface ProjectMapper {

//    CRUD ----------------------------------------------------------------------

    @Insert("INSERT INTO app_project (id ,name , description, date_create, date_update, user_id, purpose_status) VALUES (#{id},#{name},#{description},#{dateCreate}, #{dateUpdate}, #{userId}, #{purposeStatus})")
    void persist(@NotNull final Project project) throws SQLException;

    @Insert("INSERT INTO app_project (id ,name , description, date_create, date_update, user_id, purpose_status) VALUES (#{id},#{name},#{description},#{dateCreate}, #{dateUpdate}, #{userId}, #{purposeStatus})")
    @Nullable
    Project merge(@NotNull final Project project) throws SQLException;

    @Select("SELECT * FROM app_project WHERE id = #{id}")
    @Nullable
    Project findOne(@NotNull final String id) throws SQLException;

    @Select("SELECT * FROM app_project")
    @NotNull
    Collection<Project> findAll() throws SQLException;

    @Delete("DELETE FROM app_project WHERE id = #{id}")
    @Nullable
    Project remove(@NotNull final String id) throws SQLException;

    @Delete("DELETE FROM app_task")
    void removeAll() throws SQLException;

//    ALL ------------------------------------------------------------------------

    @Select("SELECT * FROM app_project WHERE id = #{id} AND user_id = #{userId}")
    @Nullable
    Project findOneByUserId(@Param("userId") @NotNull final String userId, @Param("id") @NotNull final String id) throws SQLException;

    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "dateCreate", column = "date_create"),
            @Result(property = "dateUpdate", column = "date_update"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "purposeStatus", column = "purpose_status")
    })
    @Nullable
    Collection<Project> findAllByUserId(@NotNull final String userId) throws SQLException;

    @Delete("DELETE FROM app_project WHERE id = #{id} AND user_id = #{userId}")
    void removeByUserId(@Param ("userId")@NotNull final String userId, @Param("id") @NotNull final String id) throws SQLException;

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAllByUserId(@NotNull final String userId) throws SQLException;

    @Select("SELECT * FROM app_project WHERE id = #{id}")
    @NotNull
    Collection<Project> findAllByLine(final @NotNull String userId, final @NotNull String line) throws SQLException;

//    UPDATE -----------------------------------------------------------

    @Update("UPDATE app_project SET name = #{line} WHERE id = #{id}")
    void updateName(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;

    @Update("UPDATE app_project SET description = #{line} WHERE id = #{id}")
    void updateDescription(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;

    @Update("UPDATE app_project SET date_create = #{line} WHERE id = #{id}")
    void updateDateCreate(@Param("id")@NotNull String id, @Param("line") @NotNull Date line) throws SQLException;

    @Update("UPDATE app_project SET date_update = #{line} WHERE id = #{id}")
    void updateDateUpdate(@Param("id")@NotNull String id, @Param("line") @NotNull Date line) throws SQLException;

    @Update("UPDATE app_project SET purpose_status = #{line} WHERE id = #{id}")
    void updateStatus(@Param("id")@NotNull String id, @Param("line") @NotNull String line) throws SQLException;
}

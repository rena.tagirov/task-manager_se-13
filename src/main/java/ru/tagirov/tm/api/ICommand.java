package ru.tagirov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.tagirov.tm.enumeration.Role;

public interface ICommand {

    @NotNull
    String command();

    @NotNull
    String description();

    void execute() throws Exception;

    boolean isSecure();

    @NotNull
    Role getRole();
}
